/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export;

import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public class ExportProcess {
    
    private ExportStrategy m_exportStrategy;
    
    public void setExportStrategy(ExportStrategy exportStrategy) {
        this.m_exportStrategy = exportStrategy;
    }
    
    public void beginDatasetExport(Dataset dataset) {
        this.m_exportStrategy.exportDataset(dataset);
    }
}
