/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitormoreira
 */
public class ExportFactory {
    
    private final String PACKAGE_PREFIX = "Export.Types";
    
    public static ExportFactory createInstance() {
        return new ExportFactory();
    }
    
    public List<String> exportTypes() {
        return bl.utils.ClassFinder.getClassNames(PACKAGE_PREFIX);
    }
    
    public ExportStrategy createExportStrategy(String exportStrategyName) {
        
        try {
            Class c = Class.forName(PACKAGE_PREFIX + '.' + exportStrategyName);
            ExportStrategy exportStrategy = (ExportStrategy)c.newInstance();
            
            return exportStrategy;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
