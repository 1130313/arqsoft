/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export;

import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public interface ExportStrategy {
    
    public void exportDataset(Dataset dataset);
}
