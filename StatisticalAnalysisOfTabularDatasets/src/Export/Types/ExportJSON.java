/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export.Types;

import Export.ExportStrategy;
import Model.Dataset;
import com.google.gson.Gson;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitormoreira
 */
public class ExportJSON implements ExportStrategy {

    private final String FILE_EXTENSION = ".json";

    @Override
    public void exportDataset(Dataset dataset) {
        Gson gson = new Gson();
        String fileName = dataset.getName().concat(FILE_EXTENSION);

        try (FileWriter writer = new FileWriter(fileName)) {
            gson.toJson(dataset, writer);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ExportJSON.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
