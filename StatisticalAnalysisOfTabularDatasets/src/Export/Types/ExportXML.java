/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export.Types;

import Export.ExportStrategy;
import Model.Cell;
import Model.Dataset;
import Model.Row;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author vitormoreira
 */
public class ExportXML implements ExportStrategy {

    private final String FILE_EXTENSION = ".xml";
    private final String ELEMENT_TAG_DATASET = "dataset";
    private final String ELEMENT_TAG_ROW = "row";
    private final String ELEMENT_TAG_CELL = "cell";
    private final String ATTRIBUTE_NAME = "name";
    private final String ATTRIBUTE_ID = "id";

    private void writeDataToDocBuilder(Element rootElement, Document doc, List<Row> datasetRows) {

        datasetRows.forEach((datasetRow) -> {

            // create an element for each row
            Element rowElement = doc.createElement(ELEMENT_TAG_ROW);
            rowElement.setAttribute(ATTRIBUTE_ID, String.valueOf(datasetRow.getId()));

            // create element for each cell of current row
            List<Cell> rowCells = datasetRow.getCells();
            rowCells.forEach((cell) -> {
                Element cellElement = doc.createElement(ELEMENT_TAG_CELL);
                cellElement.setAttribute(ATTRIBUTE_ID, String.valueOf(cell.getId()));
                cellElement.appendChild(doc.createTextNode(String.valueOf(cell.getValue())));
                rowElement.appendChild(cellElement);
            });
            
            rootElement.appendChild(rowElement);
        });
    }

    @Override
    public void exportDataset(Dataset dataset) {

        try {
            // create new document
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            // create root element
            String datasetName = dataset.getName();
            Element rootElement = doc.createElement(ELEMENT_TAG_DATASET);
            rootElement.setAttribute(ATTRIBUTE_NAME, datasetName);
            doc.appendChild(rootElement);

            // write dataset to document
            List<Row> datasetRows = dataset.getRows();
            this.writeDataToDocBuilder(rootElement, doc, datasetRows);

            // transform document to xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            File xmlFile = new File(datasetName.concat(FILE_EXTENSION));
            StreamResult result = new StreamResult(xmlFile);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(domSource, result);

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ExportXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(ExportXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(ExportXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
