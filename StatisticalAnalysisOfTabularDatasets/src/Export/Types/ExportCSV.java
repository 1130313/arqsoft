/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Export.Types;

import Export.ExportStrategy;
import Model.Cell;
import Model.Dataset;
import Model.Row;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitormoreira
 */
public class ExportCSV implements ExportStrategy {

    private final char DEFAULT_CELL_SEPARATOR = ';';
    private final String FILE_EXTENSION = ".csv";
    
    private void writeDataToFile(FileWriter csvWriter, List<Row> datasetRows) {
        StringBuilder stringBuilder = new StringBuilder();
        
        datasetRows.forEach((datasetRow) -> {
            List<Cell> cells = datasetRow.getCells();
            cells.forEach((cell) -> {
                stringBuilder.append(cell.getValue()).append(DEFAULT_CELL_SEPARATOR);
            });
            stringBuilder.deleteCharAt(stringBuilder.length() - 1).append("\n");
        });
        
        try {
                csvWriter.append(stringBuilder.toString());
            } catch (IOException ex) {
                Logger.getLogger(ExportCSV.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    
    @Override
    public void exportDataset(Dataset dataset) {
        
        String fileName = dataset.getName();
        fileName = fileName.concat(FILE_EXTENSION);
        
        try {
            FileWriter csvWriter = new FileWriter(fileName);
            this.writeDataToFile(csvWriter, dataset.getRows());
            csvWriter.flush();
            csvWriter.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ExportCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
