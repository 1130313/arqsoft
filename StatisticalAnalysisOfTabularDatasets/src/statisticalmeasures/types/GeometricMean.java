/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Cell;
import Model.Column;
import Model.Dataset;
import Model.Measurable;
import Model.Row;
import java.util.ArrayList;
import java.util.List;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;

/**
 *
 * @author Outros
 */
public class GeometricMean implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        List<List<Integer>> data = measurable.getData();
        int rows = data.size();
        int cols = data.get(0).size();
        int totalElements = rows * cols;
        double total = 1.0;

        for (List<Integer> cellList : data) {
            for (Integer cell : cellList) {
                total *= cell.intValue();
            }
        }
        //Nth root = 1 / n
        return Math.pow(total, (double) 1 / totalElements);
    }
}
