/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Dataset;
import Model.Measurable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;
import statisticalmeasures.utils.IntegerComparator;

/**
 *
 * @author Outros
 */
public class Median implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        List<List<Integer>> data = measurable.getData();
        int rows = data.size();
        int cols = data.get(0).size();
        int totalElements = rows * cols;
        int total = 1;
        List<Integer> dataArray = new ArrayList<>();
        for (List<Integer> line : data) {
            dataArray.addAll(line);
        }
        dataArray.sort(new IntegerComparator());
        //Se for par
        int size = dataArray.size();
        double median;
        if (size % 2 == 0) {
            int firstElement = (int) Math.floor((double) (size - 1) / 2);
            int secondElement = (int) Math.floor((double) size / 2);
            median = (double) (dataArray.get(firstElement) + dataArray.get(secondElement)) / 2;
        } //Se for impar
        else {
            int midElement = (int) Math.round((size - 1) / 2);
            median = dataArray.get(midElement);
        }
        return median;
    }
}
