/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Dataset;
import Model.Measurable;
import java.util.ArrayList;
import java.util.List;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;

/**
 *
 * @author Outros
 */
public class Variance implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        List<List<Integer>> data = measurable.getData();
        int rows = data.size();
        int cols = data.get(0).size();
        int totalElements = rows * cols;
        List<Integer> dataArray = new ArrayList<>();
        for (List<Integer> line : data) {
            dataArray.addAll(line);
        }
        int total = 0;
        for (Integer value : dataArray) {
            total += value;
        }
        if (totalElements == 0) {
            return -1;
        }
        double mean = (double) total / totalElements;

        double sumDeviationValues = 0;
        for (Integer value : dataArray) {
            sumDeviationValues += Math.pow((double) value - mean, 2);
        }
        double variance = sumDeviationValues / totalElements;
        return variance;
    }
}
