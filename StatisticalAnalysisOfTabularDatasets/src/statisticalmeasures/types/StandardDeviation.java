/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Dataset;
import Model.Measurable;
import java.util.ArrayList;
import java.util.List;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;

/**
 *
 * @author Outros
 */
public class StandardDeviation implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        Variance varianceCalc = new Variance();
        double variance = varianceCalc.calculateStatisticalMeasure(measurable);
        double standardDeviation = Math.sqrt(variance);
        return standardDeviation;
    }
}
