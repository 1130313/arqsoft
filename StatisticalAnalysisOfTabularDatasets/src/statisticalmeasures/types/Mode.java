/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Dataset;
import Model.Measurable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;

/**
 *
 * @author Outros
 */
public class Mode implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        List<List<Integer>> data = measurable.getData();
        List<Integer> dataArray = new ArrayList<>();
        for (List<Integer> line : data) {
            dataArray.addAll(line);
        }

        List<Integer> modes = new ArrayList<>();
        int max = -1;
        Map<Integer, Integer> countMap = new HashMap<>();
        for (Integer value : dataArray) {
            int count = 0;

            if (countMap.containsKey(value)) {
                count = countMap.get(value) + 1;
            } else {
                count = 1;
            }

            countMap.put(value, count);

            if (count > max) {
                max = count;
            }
        }
        for (final Map.Entry<Integer, Integer> tuple : countMap.entrySet()) {
            if (tuple.getValue() == max) {
                modes.add(tuple.getKey());
            }
        }
        return modes.get(0);
    }
}
