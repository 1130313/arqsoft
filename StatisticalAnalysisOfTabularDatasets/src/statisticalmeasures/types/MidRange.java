/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures.types;

import Model.Dataset;
import Model.Measurable;
import java.util.ArrayList;
import java.util.List;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;
import statisticalmeasures.utils.IntegerComparator;

/**
 *
 * @author Outros
 */
public class MidRange implements StatisticalMeasureStrategy {

    @Override
    public double calculateStatisticalMeasure(Measurable measurable) {
        List<List<Integer>> data = measurable.getData();
        int rows = data.size();
        int cols = data.get(0).size();
        int totalElements = rows * cols;
        int total = 1;

        int size = data.size();
        double midRange;

        if (size == 1 && data.get(0).size() == 1) {
            midRange = (double) data.get(0).get(0);
        } else {
            List<Integer> dataArray = new ArrayList<>();
            for (List<Integer> line : data) {
                dataArray.addAll(line);
            }
            dataArray.sort(new IntegerComparator());
            int min = dataArray.get(0);
            int max = dataArray.get(dataArray.size() - 1);
            midRange = (double) (min + max) / 2;
        }
        return midRange;
    }
}
