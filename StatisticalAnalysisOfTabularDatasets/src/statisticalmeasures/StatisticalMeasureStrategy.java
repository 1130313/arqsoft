/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures;

import Model.Measurable;

/**
 *
 * @author Outros
 */
public interface StatisticalMeasureStrategy {
    public double calculateStatisticalMeasure(Measurable measurable);
}
