/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalmeasures;

import Export.ExportFactory;
import Export.ExportStrategy;
import bl.utils.ClassFinder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Outros
 */
public class StatisticalMeasureFactory {
 private final String PACKAGE_PREFIX = "statisticalmeasures.types.";
 private final String PACKAGE = "statisticalmeasures.types";
    
    public static StatisticalMeasureFactory createInstance() {
        return new StatisticalMeasureFactory();
    }
    public List<String> getAvailableStatisticalMeasures()
    {
        return ClassFinder.getClassNames(PACKAGE);
    }
            
    public StatisticalMeasureStrategy createStatisticalMeasureStrategy(String statisticalMeasureStrategyName) {
        
        try {
            Class c = Class.forName(PACKAGE_PREFIX + statisticalMeasureStrategyName);
            StatisticalMeasureStrategy statisticalMeasureStrategy = (StatisticalMeasureStrategy)c.newInstance();
            return statisticalMeasureStrategy;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ExportFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
