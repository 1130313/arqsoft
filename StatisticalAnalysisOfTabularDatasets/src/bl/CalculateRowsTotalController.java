/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Model.Dataset;
import Model.Row;

/**
 *
 * @author Outros
 */
class CalculateRowsTotalController {

    public CalculateRowsTotalController() {
    }

    int calculateRowsTotal(Dataset d, int choosedRow) {
       Row r = d.getRows().get(choosedRow);
       return r.calculteTotal();
    }
    
}
