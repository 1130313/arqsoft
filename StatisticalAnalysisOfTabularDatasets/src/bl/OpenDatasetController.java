/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Model.Dataset;
import java.util.List;
import persistence.DatasetRepository;
import persistence.Persistence;
import persistence.RepositoryFactory;

/**
 *
 * @author Outros
 */
public class OpenDatasetController {
    
    List<Dataset> getAll() {
        RepositoryFactory fact = Persistence.getRepositoryFactory();
        DatasetRepository repo = fact.getDatasetRepository();
        List<Dataset> datasets = repo.getAll();
        return datasets;
    }
    
}
