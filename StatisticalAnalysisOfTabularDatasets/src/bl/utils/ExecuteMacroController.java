/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl.utils;

import Macros.Macro;
import Macros.MacroRuner;
import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public class ExecuteMacroController {
    
    public Dataset executeMacro(Macro m) {
        MacroRuner macroRuner = MacroRuner.createInstance();
        return macroRuner.startMacro(m);
    }
}
