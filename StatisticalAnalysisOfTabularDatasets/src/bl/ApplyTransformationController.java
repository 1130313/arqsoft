/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationFactory;
import Transformations.TransformationProcess;

/**
 *
 * @author vitormoreira
 */
public class ApplyTransformationController {
    
    private final TransformationProcess m_transformation_process;

    public ApplyTransformationController() {
        this.m_transformation_process = new TransformationProcess();
    }
    
    public Dataset performTransformation(String transformationType, Dataset dataset, Object data) {
        TransformationFactory factory = TransformationFactory.createInstance();
        this.m_transformation_process.setTransformationStrategy(factory.createTransformationStrategy(transformationType));
        Parameter transformationData = new Parameter(dataset);
        transformationData.addParameter(data);
        return this.m_transformation_process.performTransformation(transformationData);
    }
}
