/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Macros.Macro;
import Model.Dataset;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author vitormoreira
 */
public class DefineMacroController {
    
    public boolean defineMacro(List<Entry<String, Object>> macroData, Dataset dataset) {
        // create macro
        Macro macro = new Macro(dataset);
        // create and add macro data
        macro.addTransformationEntries(macroData);
        
        // save macro
        // adicionar aqui a parte de gravar o objeto na base de dados
        return true;
    }
}
