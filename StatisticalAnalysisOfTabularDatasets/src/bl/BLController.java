/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import DTO.DatasetAssembler;
import DTO.DatasetDTO;
import DTO.DatasetDTOCount;
import Export.ExportFactory;
import Macros.Macro;
import Model.Countable;
import Model.Dataset;
import Model.Measurable;
import Transformations.TransformationFactory;
import bl.utils.ExecuteMacroController;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import statisticalmeasures.StatisticalMeasureFactory;
import statisticalmeasures.StatisticalMeasureStrategy;

/**
 *
 * @author Outros
 */
public class BLController {

    public enum TYPE {

        DATASET, ROW, COLUMN
    };

    public Long saveDataset(DatasetDTO dto) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(dto);
        SaveDatasetController controller = new SaveDatasetController();
        boolean returnValue = controller.saveDataset(dataset, dto.getId());
        if(returnValue)
            return dataset.getId();
        else
            return dto.getId();
    }

    public List<DatasetDTO> getAvailableDatasets() {
        OpenDatasetController controller = new OpenDatasetController();
        List<Dataset> datasets = controller.getAll();
        List<DatasetDTO> datasetsDTO = new ArrayList<>();
        DatasetAssembler assemblerDatasetToDTO = new DatasetAssembler();
        for (Dataset dataset : datasets) {
            DatasetDTO dto = assemblerDatasetToDTO.transformDatasetIntoDTO(dataset);
            datasetsDTO.add(dto);
        }
        return datasetsDTO;
    }

    public int calculateRowsTotal(DatasetDTO dto, int choosedRow) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset d = assembler.transformDatasetDTOIntoDataset(dto);
        CalculateRowsTotalController controller = new CalculateRowsTotalController();
        int total = controller.calculateRowsTotal(d, choosedRow);
        return total;
    }

    public int calculateColumnsTotal(DatasetDTO dto, int choosedColumn) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset d = assembler.transformDatasetDTOIntoDataset(dto);
        CalculateColumnsTotalController controller = new CalculateColumnsTotalController();
        int total = controller.calculateColumnsTotal(d, choosedColumn);
        return total;

    }

    public int numberOfElements(TYPE type, DatasetDTOCount dto) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(dto.getDatasetDTO());
        int number = 0;
        Countable c;
        switch (type) {
            case DATASET: {
                c = dataset;
                number = c.countNumberOfElements();
                break;
            }
            case ROW: {
                c = dataset.getRows().get(dto.getRowNumber());
                number = c.countNumberOfElements();
                break;
            }
            case COLUMN: {
                c = dataset.getColumns().get(dto.getColumnNumber());
                number = c.countNumberOfElements();
                break;
            }

        }
        return number;

    }

    public List<String> getAvailableExportTypes() {
        ExportFactory factory = ExportFactory.createInstance();
        return factory.exportTypes();
    }

    public List<String> getAvailableStatisticalMeasures() {
        StatisticalMeasureFactory factory = StatisticalMeasureFactory.createInstance();
        return factory.getAvailableStatisticalMeasures();
    }

    public void exportDataset(String choosenExportType, DatasetDTO dto) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(dto);
        ExportDatasetController controller = new ExportDatasetController();
        controller.exportDataset(choosenExportType, dataset);
    }
    
    public DatasetDTO importDataset(File choosenImportFile) throws Exception {
        ImportDatasetController controller = new ImportDatasetController();
        Dataset importedDataset = controller.importDataset(choosenImportFile);
        if(importedDataset != null) {
            DatasetAssembler assembler = new DatasetAssembler();
            return assembler.transformDatasetIntoDTO(importedDataset);
        }
        
        return null;
    }
    
    public double calculateStatisticalMeasure(TYPE type, DatasetDTOCount dto, String choosenStatisticalMeasure) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(dto.getDatasetDTO());

        StatisticalMeasureFactory factory = StatisticalMeasureFactory.createInstance();
        StatisticalMeasureStrategy strat = factory.createStatisticalMeasureStrategy(choosenStatisticalMeasure);

        Measurable measurable;
        double result = 0;
        switch (type) {
            case DATASET: {
                measurable = dataset;
                result = strat.calculateStatisticalMeasure(measurable);
                break;
            }
            case ROW: {
                measurable = dataset.getRows().get(dto.getRowNumber());
                result = strat.calculateStatisticalMeasure(measurable);
                break;
            }
            case COLUMN: {
                measurable = dataset.getColumns().get(dto.getColumnNumber());
                result = strat.calculateStatisticalMeasure(measurable);
                break;
            }
        }

        return result;
    }
    public List<String> getAvailableTransformations() {
        TransformationFactory factory = TransformationFactory.createInstance();
        return factory.transformationTypes();
    }
    
    public List<String> getAvailableLinearInterpolationTypes() {
        TransformationFactory factory = TransformationFactory.createInstance();
        return factory.linearInterpolationTypes();
    }
    
    public DatasetDTO executeTransformation(String transformationType, DatasetDTO datasetDTO, Object data) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(datasetDTO);
        ApplyTransformationController controller = new ApplyTransformationController();
        Dataset newDataset = controller.performTransformation(transformationType, dataset, data);
        DatasetDTO dto = assembler.transformDatasetIntoDTO(newDataset);
        return dto;
    }
    
    public boolean defineMacro(List<Entry<String,Object>> macroData, DatasetDTO datasetDTO) {
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset dataset = assembler.transformDatasetDTOIntoDataset(datasetDTO);
        DefineMacroController controller = new DefineMacroController();
        return controller.defineMacro(macroData, dataset);
    }
    
    public DatasetDTO executeMacro(Macro m) {
        ExecuteMacroController controller = new ExecuteMacroController();
        Dataset dataset = controller.executeMacro(m);
        if(dataset != null) {
            DatasetAssembler assembler = new DatasetAssembler();
            DatasetDTO dto = assembler.transformDatasetIntoDTO(dataset);
            return dto;
        }
        return null;
    }
}
