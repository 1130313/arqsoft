/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Model.Dataset;
import persistence.DatasetRepository;
import persistence.Persistence;
import persistence.RepositoryFactory;

/**
 *
 * @author Outros
 */
public class SaveDatasetController {

    boolean saveDataset(Dataset dataset, Long id) {
        
        RepositoryFactory fact = Persistence.getRepositoryFactory();
        DatasetRepository repo = fact.getDatasetRepository();
        if(id!=null)
        {
            repo.update(dataset,id);
            return false;
        }
        repo.add(dataset);
        return true;
    }
}
