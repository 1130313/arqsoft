/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Export.ExportFactory;
import Export.ExportProcess;
import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public class ExportDatasetController {
    
    private final ExportProcess m_export_process;
    
    ExportDatasetController() {
        this.m_export_process = new ExportProcess();
    }
    
    public void exportDataset(String exportType, Dataset dataset) {
        ExportFactory factory = ExportFactory.createInstance();
        this.m_export_process.setExportStrategy(factory.createExportStrategy(exportType));
        this.m_export_process.beginDatasetExport(dataset);
    }
}
