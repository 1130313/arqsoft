/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Import.ImportFactory;
import Import.ImportProcess;
import Import.utils.ImportUtils;
import Model.Dataset;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author vitormoreira
 */
public class ImportDatasetController {
    
    private final ImportProcess m_importProcess;
    
    public ImportDatasetController() {
        this.m_importProcess = new ImportProcess();
    }
    
    public Dataset importDataset(File choosenImportFile) throws Exception {
        String fileName = choosenImportFile.getName();
        String fileExt = ImportUtils.getFileExtension(fileName);
        if(fileExt != null) {
            String importStrategyName = ImportUtils.ImportFileType.retrieveImportClassName(fileExt);
            if(importStrategyName != null) {
                ImportFactory factory = ImportFactory.createInstance();
                this.m_importProcess.setImportStrategy(factory.createExportStrategy(importStrategyName));
                return this.m_importProcess.beginImportOfDataset(choosenImportFile);
            } 
        }
        
        return null;
    }
    
}
