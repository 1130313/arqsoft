/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import Model.Column;
import Model.Dataset;

/**
 *
 * @author Outros
 */
class CalculateColumnsTotalController {

    int calculateColumnsTotal(Dataset d, int choosedColumn) {
        Column c = d.getColumns().get(choosedColumn);
        return c.calculateTotal();
    }
    
}
