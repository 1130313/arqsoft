/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations;

import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public class ParameterBuilder {
    
    public ParameterBuilder createInstance() {
        return new ParameterBuilder();
    }
    
    private Parameter createParameterScalar(Dataset dataset, int scalar) {
        Parameter parameter = new Parameter(dataset);
        parameter.addParameter(scalar);
        return parameter;
    }
}
