/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations;

import Transformations.utils.LinearInterpolationTypes;
import bl.utils.ClassFinder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitormoreira
 */
public class TransformationFactory {

    private final String PACKAGE_PREFIX = "Transformations.Types";
    
    /**
     * Returns a new instance of TransformationFactory
     * @return TransformationStrategy
     */
    public static TransformationFactory createInstance() {
        return new TransformationFactory();
    }
    
    public List<String> transformationTypes() {
        return ClassFinder.getClassNames(PACKAGE_PREFIX);
    }
    
    public List<String> linearInterpolationTypes() {
        LinearInterpolationTypes.LinearInterpolationType[] lITypes = LinearInterpolationTypes.LinearInterpolationType.values();
        List<String> lITypesNames = new ArrayList<>();
        for (LinearInterpolationTypes.LinearInterpolationType lIType : lITypes) {
            lITypesNames.add(lIType.toString());
        }
        
        return lITypesNames;
    }

    public TransformationStrategy createTransformationStrategy(String name) {
        
        try {
            Class c = Class.forName(PACKAGE_PREFIX + '.' + name);
            TransformationStrategy strategy = (TransformationStrategy)c.newInstance();
            
            return strategy;
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TransformationFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TransformationFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TransformationFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
