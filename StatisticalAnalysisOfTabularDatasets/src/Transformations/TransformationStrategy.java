/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations;

import Model.Dataset;

/**
 *
 * @author vitormoreira
 */
public interface TransformationStrategy {
            
    public Dataset performTransformation(Parameter transformationData);
}
