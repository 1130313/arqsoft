/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations;

import Model.Dataset;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class Parameter {
    
    private Dataset m_main_dataset;
    private List<Object> m_parameters;
    
    public Parameter() {
        this.m_parameters = new ArrayList<>();
    }
    
    public Parameter(Dataset mainDataset) {
        this.m_main_dataset = mainDataset;
        this.m_parameters = new ArrayList<>();
    }
    
    public void setMainDataset(Dataset d) {
        this.m_main_dataset = d;
    }
    
    public void addParameter(Object parameter) {
        this.m_parameters.add(parameter);
    }
    
    public List<Object> retrieveData() {
        return this.m_parameters;
    }
    
    public Dataset retrieveMainDataset() {
        return this.m_main_dataset;
    }
}
