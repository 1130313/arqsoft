/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations.Types;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import Transformations.utils.LinearInterpolationTypes.LinearInterpolationType;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class LinearInterpolation implements TransformationStrategy {

    @Override
    public Dataset performTransformation(Parameter transformationData) {
        // get transformation data
        List<Object> parameters = transformationData.retrieveData();
        Dataset dataset = transformationData.retrieveMainDataset();
        String typeString = null;
        LinearInterpolationType type = null;

        for (Object param : parameters) {
            if (param instanceof String) {
                typeString = (String) param;
            }
        }

        if (typeString != null) {
            type = LinearInterpolationType.valueOf(typeString);
        }

        // check arguments
        if (parameters.isEmpty() || dataset == null || type == null) {
            throw new IllegalArgumentException("Invalid parameters for this transformation!");
        }

        int n_rows = dataset.getRowsSize();
        int n_columns = dataset.getColumnsSize();
        List<List<Integer>> data = dataset.getData();

        switch (type) {
            case ROWS:
                int n_newdataset_rows = (n_rows * 2) - 1;
                Dataset newDataset = new Dataset(n_newdataset_rows, n_columns);
                List<List<Integer>> newDatasetData = newDataset.getData();

                // fill first row
                List<Integer> row_zero = data.get(0);
                for (int k = 0; k < n_columns; k++) {
                    newDatasetData.get(0).set(k, row_zero.get(k));
                }

                int c, k;
                for (int i = 2; i < n_newdataset_rows; i += 2) {
                    c = i / 2;
                    List<Integer> row = data.get(c);
                    // copy row
                    for (int j = 0; j < n_columns; j++) {
                        newDatasetData.get(i).set(j, row.get(j));
                    }

                    // interpolate
                    k = 0;
                    for (Integer value : newDatasetData.get(i)) {
                        newDatasetData.get(i - 1).set(k, Math.round((value + data.get(c - 1).get(k)) / 2));
                        k++;
                    }
                }

                // fill and set name
                newDataset.fillDataset(newDatasetData);
                newDataset.setName(dataset.getName() + "LinearIntRows");

                return newDataset;
            
            case COLS:
                int n_newdataset_columns = (n_columns * 2) - 1;
                Dataset nDataset = new Dataset(n_rows, n_newdataset_columns);
                List<List<Integer>> nDatasetData = nDataset.getData();
                
                // copy the data from old dataset
                int i, j = 0;
                for (List<Integer> row : nDatasetData) {
                    i = 0;
                    for (Integer value : data.get(j)) {
                        row.set(i * 2, value);
                        i++;
                    }
                    j++;
                }
                
                // interpolate
                for (List<Integer> list : nDatasetData) {
                    for (i = 2; i < n_newdataset_columns; i += 2) {
                        list.set(i - 1, Math.round((list.get(i - 2) + list.get(i)) / 2));
                    }
                }
                
                // fill and set name
                nDataset.fillDataset(nDatasetData);
                nDataset.setName(dataset.getName() + "LinearIntCols");
                
                return nDataset;
               
        }

        return null; // it will NEVER get here
    }
}
