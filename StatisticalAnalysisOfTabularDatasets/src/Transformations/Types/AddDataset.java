/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations.Types;

import DTO.DatasetAssembler;
import DTO.DatasetDTO;
import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class AddDataset implements TransformationStrategy {

    @Override
    public Dataset performTransformation(Parameter transformationData) {
        // get transformation data
        List<Object> parameters = transformationData.retrieveData();
        Dataset dataset = transformationData.retrieveMainDataset();
        DatasetDTO secondDatasetDTO = null;
        
        for (Object param : parameters) {
            if (param instanceof DatasetDTO) {
                secondDatasetDTO = (DatasetDTO)param;
            }
        }
        
        // check arguments
        if (parameters.isEmpty() || dataset == null || secondDatasetDTO == null) {
            throw new IllegalArgumentException("Invalid parameters for this transformation!");
        }
        
        // transform the second dataset dto into a dataset
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset secondDataset = assembler.transformDatasetDTOIntoDataset(secondDatasetDTO);
        
        // check if they are the same size
        int n_rows = dataset.getRowsSize();
        int n_columns = dataset.getColumnsSize();
        if (n_rows != secondDataset.getRowsSize()
                || n_columns != secondDataset.getColumnsSize()) {
            throw new IllegalArgumentException("The datasets must be of same size!");
        }
        
        // add each value with the second dataset value
        List<List<Integer>> data = dataset.getData();
        List<List<Integer>> secondData = secondDataset.getData();
        
        int i = 0, j;
        for (List<Integer> row : data) {
            j = 0;
            List<Integer> secondRow = secondData.get(i);
            for (Integer value : row) {
                row.set(j, value + secondRow.get(j));
                j++;
            }
            i++;
        }
        
        // create new dataset with the same size 
        Dataset newDataset = new Dataset(n_rows, n_columns);
        
        // fill the new dataset
        newDataset.fillDataset(data);
        newDataset.setName(dataset.getName() + "addedWith" + secondDataset.getName());
        
        return newDataset;
    }
    
}
