/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations.Types;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class Transpose implements TransformationStrategy {

    @Override
    public Dataset performTransformation(Parameter transformationData) {
        // get transformation data
        Dataset dataset = transformationData.retrieveMainDataset();
        
        // check arguments
        if (dataset == null) {
            throw new IllegalArgumentException("Invalid parameters for this transformation!");
        }
        
        int n_rows = dataset.getRowsSize();
        int n_columns = dataset.getColumnsSize();
        
        // create new dataset
        Dataset newDataset = new Dataset(n_columns, n_rows);
        
        // transpose
        List<List<Integer>> datasetData = dataset.getData();
        List<List<Integer>> newDatasetData = newDataset.getData();
        for (int i = 0; i < n_rows; i++)
            for (int j = 0; j < n_columns; j++)
                newDatasetData.get(j).set(i, datasetData.get(i).get(j));
        
        // fill data set and name it
        newDataset.fillDataset(newDatasetData);
        newDataset.setName(dataset.getName() + "Transposed");
        
        return newDataset;
        
    }
    
}
