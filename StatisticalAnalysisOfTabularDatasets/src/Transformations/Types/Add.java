/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations.Types;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class Add implements TransformationStrategy {

    @Override
    public Dataset performTransformation(Parameter transformationData) {
        
        // get transformation data
        List<Object> parameters = transformationData.retrieveData();
        Dataset dataset = transformationData.retrieveMainDataset();
        int scalar = 0;
        
        for (Object param : parameters) {
            if (param instanceof Integer) {
                scalar = (Integer)param;
            }
        }
        
        // check arguments
        if (parameters.isEmpty() || dataset == null || scalar == 0) {
            throw new IllegalArgumentException("Invalid parameters for this transformation!");
        }
        
        // add a scalar to each value
        List<List<Integer>> data = dataset.getData();
        int i;
        for (List<Integer> row : data) {
            i = 0;
            for (Integer value : row) {
                row.set(i, value+scalar);
                i++;
            }
        }
        
        // create new dataset with the same size 
        int n_rows = dataset.getRowsSize();
        int n_columns = dataset.getColumnsSize();
        Dataset newDataset = new Dataset(n_rows, n_columns);
        
        // fill the new dataset
        newDataset.fillDataset(data);
        newDataset.setName(dataset.getName() + "added");
        
        return newDataset;
    }
    
}
