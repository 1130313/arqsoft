/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformations.Types;

import DTO.DatasetAssembler;
import DTO.DatasetDTO;
import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class MultiplyDataset implements TransformationStrategy {

    @Override
    public Dataset performTransformation(Parameter transformationData) {
        // get transformation data
        List<Object> parameters = transformationData.retrieveData();
        Dataset dataset = transformationData.retrieveMainDataset();
        DatasetDTO secondDatasetDTO = null;
        
        for (Object param : parameters) {
            if (param instanceof DatasetDTO) {
                secondDatasetDTO = (DatasetDTO)param;
            }
        }
        
        // check arguments
        if (parameters.isEmpty() || dataset == null || secondDatasetDTO == null) {
            throw new IllegalArgumentException("Invalid parameters for this transformation!");
        }
        
        // transform the second dataset dto into a dataset
        DatasetAssembler assembler = new DatasetAssembler();
        Dataset secondDataset = assembler.transformDatasetDTOIntoDataset(secondDatasetDTO);
        
        // check if they are the same size
        int n_rows = dataset.getRowsSize();
        int n_rows_second_dataset = secondDataset.getRowsSize();
        int n_columns = dataset.getColumnsSize();
        int n_columns_second_dataset = secondDataset.getColumnsSize();
        if (n_columns != n_rows_second_dataset) {
            throw new IllegalArgumentException("The number of columns of the first dataset must"
                    + " be equal to the number of rows of the second dataset!");
        }
        
        // create new dataset
        Dataset newDataset = new Dataset(n_rows, n_columns_second_dataset);
        
        // multiplication of datasets
        List<List<Integer>> newDatasetData = newDataset.getData();
        List<List<Integer>> datasetData = dataset.getData();
        List<List<Integer>> secondDatasetData = secondDataset.getData();
        
        for (int i = 0; i < n_rows; i++)
            for (int j = 0; j < n_columns_second_dataset; j++)
                for (int k = 0; k < n_columns; k++)
                    newDatasetData.get(i).set(j, newDatasetData.get(i).get(j) + 
                            (datasetData.get(i).get(k) * secondDatasetData.get(k).get(j)));
        
        // fill new dataset and change the name
        newDataset.fillDataset(newDatasetData);
        newDataset.setName(dataset.getName() + "MUL" + secondDataset.getName());
        
        return newDataset;
    }
    
}
