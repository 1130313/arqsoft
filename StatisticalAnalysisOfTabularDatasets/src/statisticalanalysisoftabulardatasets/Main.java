/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statisticalanalysisoftabulardatasets;

import Model.Dataset;
import java.awt.BorderLayout;
import javafx.scene.control.SelectionMode;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellEditor;
import presentation.controller.DatasetController;
import presentation.model.DatasetModel;
import presentation.view.DatasetView;

/**
 *
 * @author Outros
 */
public class Main {

    /**
     * @param args the command line arguments //
     */
    public static void main(String[] args) {

        DatasetModel model = new DatasetModel();
        DatasetController controller = new DatasetController(model);
       
    }
}
