/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Macros;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationStrategy;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author vitormoreira
 */
public class MacroRuner {
    
    public static MacroRuner createInstance() {
        return new MacroRuner();
    }
    
    public Dataset startMacro(Macro m) {
        List<Entry<TransformationStrategy, Parameter>> steps = m.retrieveMacroSteps();
        if(!steps.isEmpty()) {
            // set inicial dataset
            Dataset newDataset = m.retrieveBaseDataset();
            steps.get(0).getValue().setMainDataset(newDataset);
            
            // loop through each transformation
            for (Entry<TransformationStrategy, Parameter> step : steps) {
                step.getValue().setMainDataset(newDataset);
                newDataset = step.getKey().performTransformation(step.getValue());
            }
            
            return newDataset;
        }
        
        return null;
    }
}
