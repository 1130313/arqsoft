/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Macros;

import Model.Dataset;
import Transformations.Parameter;
import Transformations.TransformationFactory;
import Transformations.TransformationStrategy;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author vitormoreira
 */
public class Macro {
    
    private Dataset m_base_dataset;
    private List<Entry<TransformationStrategy,Parameter>> m_transformations;
    
    public Macro(Dataset baseDataset) {
        this.m_base_dataset = baseDataset;
        this.m_transformations = new ArrayList<>();
    }
    
    private void addTransformation(Entry<TransformationStrategy,Parameter> transformation) {
        this.m_transformations.add(transformation);
    }
    
    public boolean addTransformationEntries(List<Entry<String, Object>> macroData) {
        TransformationFactory factory = TransformationFactory.createInstance();
        TransformationStrategy strategy;
        Parameter parameter;
        
        // correct entry format for macro
        Entry<TransformationStrategy, Parameter> newTransformationForMacro;
        
        // for each entry get the strategy and the parameter
        // then add it to macros' list
        for (Entry<String, Object> entry : macroData) {
            strategy = factory.createTransformationStrategy(entry.getKey());
            parameter = new Parameter();
            parameter.addParameter(entry.getValue());
            newTransformationForMacro = new AbstractMap.SimpleEntry<>(strategy, parameter);
            this.addTransformation(newTransformationForMacro);
        }
        
        return true;
    }
    
    public Dataset retrieveBaseDataset() {
        return this.m_base_dataset;
    }
    
    public List<Entry<TransformationStrategy,Parameter>> retrieveMacroSteps() {
        return this.m_transformations;
    }
}
