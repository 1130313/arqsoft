/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view;

import DTO.DatasetDTO;
import bl.BLController.TYPE;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import presentation.controller.DatasetController;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import presentation.controller.EnterDatasetController;
import presentation.model.DatasetModel;
import presentation.model.EnterDatasetModel;
import presentation.view.components.MyJTable;
import presentation.view.components.MyTableModel;
import presentation.view.components.RowNumberTable;

/**
 *
 * @author Outros
 */
public class DatasetView implements Observer, ActionListener {

    public String APPLICATION_NAME = "Statistical Analysis of tabular datasets";
    //private DatasetModel

    private DatasetController m_controller;
    private DatasetModel m_model;

    //window to display this view
    private JFrame window;

    //menu bar
    private JMenuBar m_menu_bar;

    //menu with file options
    private JMenu m_menu_file;
    private JMenuItem m_file_import;
    private JMenuItem m_file_export;

    //number of elements
    private JMenu m_menu_show_number_of_elements;
    private JMenuItem m_show_dataset;
    private JMenuItem m_show_selected_row;
    private JMenuItem m_show_selected_column;

    //menu with dataset options
    private JMenu m_menu_dataset;
    private JMenuItem m_dataset_open;
    private JMenuItem m_dataset_enter;
    private JMenuItem m_dataset_save;

    //menu with calculate row's total and column's total
    private JMenu m_menu_calculate;
    private JMenuItem m_calculate_rows_total;
    private JMenuItem m_calculate_columns_total;

    //menu with statistical measures
    private JMenu m_menu_statistical_measures;
    private JMenuItem m_statistical_measures_dataset;
    private JMenuItem m_statistical_measures_selected_row;
    private JMenuItem m_statistical_measures_selected_column;

    //menu with transformations
    private JMenuItem m_menu_transformation;

    //menu with plots
    private JMenu m_menu_plot;

    //all plot in pie possibilities
    private JMenu m_plot_pie;
    private JMenuItem m_pie_col;
    private JMenuItem m_pie_row;

    private JMenu m_plot_line_bar;

    //all plot in line possibilities
    private JMenu m_plot_line;
    private JMenuItem m_line_col;
    private JMenuItem m_line_row;
    private JMenuItem m_line_dataset;

    //all plot in bar 
    private JMenu m_plot_bar;
    private JMenuItem m_bar_col;
    private JMenuItem m_bar_row;
    private JMenuItem m_bar_dataset;
    
    private JMenu m_macros;
    private JMenuItem m_macros_define;
    private JMenuItem m_macros_load;
    private JMenuItem m_macros_execute;

    private JTabbedPane m_datasets_container;

    //List<JScrollPane> m_scroll_panes_dataset;
    List<JTable> m_datasets;
    //Tab counter
    int count;

    public DatasetView(DatasetController controller, DatasetModel model) {
        this.m_controller = controller;
        this.m_model = model;
        this.m_model.addObserver((Observer) this);
        this.count = 0;
    }

    public void createView() {
        //create a window and render it
        window = new JFrame(APPLICATION_NAME);
        window.setJMenuBar(m_menu_bar);
        window.setLayout(new BorderLayout());
        window.add(m_datasets_container, BorderLayout.CENTER);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);

    }

    public void createControls() {
        //this.m_scroll_panes_dataset = new ArrayList<>();
        this.m_datasets = new ArrayList<>();
        createJMenuBar();
        createJTabbedPane();
    }

    private void createJMenuBar() {
        this.m_menu_bar = new JMenuBar();
        createJMenuFile();
        createJMenuShowNumberOfElements();
        createJMenuCalculate();
        createJMenuDataset();
        createJMenuStatisticalMeasures();
        createJMenuTransformation();
        createJMenuPlot();
        createJMenuMacros();
    }

    private void createJMenuFile() {
        this.m_menu_file = new JMenu("File");

        this.m_file_import = new JMenuItem("Import");
        this.m_file_import.addActionListener(this);

        this.m_file_export = new JMenuItem("Export");
        this.m_file_export.addActionListener(this);

        this.m_menu_file.add(m_file_import);

        this.m_menu_file.add(m_file_export);

        this.m_menu_bar.add(m_menu_file);
    }

    private void createJMenuShowNumberOfElements() {
        this.m_menu_show_number_of_elements = new JMenu("Show number of elements");

        this.m_show_dataset = new JMenuItem("Dataset");
        this.m_show_dataset.addActionListener(this);
        this.m_menu_show_number_of_elements.add(m_show_dataset);

        this.m_show_selected_row = new JMenuItem("Selected row");
        this.m_show_selected_row.addActionListener(this);
        this.m_menu_show_number_of_elements.add(m_show_selected_row);

        this.m_show_selected_column = new JMenuItem("Selected column");
        this.m_show_selected_column.addActionListener(this);
        this.m_menu_show_number_of_elements.add(this.m_show_selected_column);

        this.m_menu_bar.add(this.m_menu_show_number_of_elements);
    }

    private void createJMenuCalculate() {
        this.m_menu_calculate = new JMenu("Calculate");

        this.m_calculate_rows_total = new JMenuItem("Row's total");
        this.m_calculate_rows_total.addActionListener(this);
        this.m_menu_calculate.add(this.m_calculate_rows_total);

        this.m_calculate_columns_total = new JMenuItem("Column's total");
        this.m_calculate_columns_total.addActionListener(this);
        this.m_menu_calculate.add(this.m_calculate_columns_total);

        this.m_menu_bar.add(this.m_menu_calculate);
    }

    private void createJMenuDataset() {
        this.m_menu_dataset = new JMenu("Dataset");

        this.m_dataset_enter = new JMenuItem("Enter");
        //add action listener to it to pass to the controller
        this.m_dataset_enter.addActionListener(this);

        this.m_dataset_open = new JMenuItem("Open");
        //add action listener to it to pass to the controller
        this.m_dataset_open.addActionListener(this);

        this.m_dataset_save = new JMenuItem("Save");
        this.m_dataset_save.addActionListener(this);
        //add action listener to it to pass to the controller

        this.m_menu_dataset.add(m_dataset_enter);
        this.m_menu_dataset.add(m_dataset_open);
        this.m_menu_dataset.add(m_dataset_save);

        this.m_menu_bar.add(m_menu_dataset);
    }

    private void createJMenuStatisticalMeasures() {
        this.m_menu_statistical_measures = new JMenu("Statistical Measures");
        //loop to create all statistical measures based on existing ones as JMenuItems

        this.m_statistical_measures_dataset = new JMenuItem("Dataset");
        this.m_statistical_measures_dataset.addActionListener(this);
        this.m_menu_statistical_measures.add(m_statistical_measures_dataset);

        this.m_statistical_measures_selected_row = new JMenuItem("Selected row");
        this.m_statistical_measures_selected_row.addActionListener(this);
        this.m_menu_statistical_measures.add(m_statistical_measures_selected_row);

        this.m_statistical_measures_selected_column = new JMenuItem("Selected column");
        this.m_statistical_measures_selected_column.addActionListener(this);
        this.m_menu_statistical_measures.add(this.m_statistical_measures_selected_column);

        this.m_menu_bar.add(this.m_menu_statistical_measures);
    }

    private void createJMenuTransformation() {
        this.m_menu_transformation = new JMenuItem("Transformation");
        this.m_menu_transformation.addActionListener(this);

        //loop to create all tranformations based on existing ones as JMenuItems
        this.m_menu_bar.add(this.m_menu_transformation);
    }

    private void createJMenuPlot() {
        this.m_menu_plot = new JMenu("Plot");
        createJMenuPieChart();
        createJMenuLineBarChart();
    }

    private void createJMenuPieChart() {
        this.m_plot_pie = new JMenu("Pie Chart");

        this.m_pie_col = new JMenuItem("Column");
        this.m_pie_col.addActionListener(this);
        //add action listener;
        this.m_pie_row = new JMenuItem("Row");
        this.m_pie_row.addActionListener(this);
        //add action listener;

        this.m_plot_pie.add(m_pie_col);
        this.m_plot_pie.add(m_pie_row);

        this.m_menu_plot.add(m_plot_pie);
    }

    private void createJMenuLineBarChart() {
        //creation of the line bar
        this.m_plot_line_bar = new JMenu("Line/Bar");
        createJMenuLineChart();
        createJMenuBarChart();
    }

    private void createJMenuLineChart() {
        //creation of line
        this.m_plot_line = new JMenu("Line");

        this.m_line_col = new JMenuItem("Col");
        this.m_line_col.addActionListener(this);
        this.m_plot_line.add(this.m_line_col);

        this.m_line_row = new JMenuItem("Row");
        this.m_line_row.addActionListener(this);
        this.m_plot_line.add(this.m_line_row);

        this.m_line_dataset = new JMenuItem("Dataset");
        this.m_line_dataset.addActionListener(this);
        this.m_plot_line.add(this.m_line_dataset);

        this.m_plot_line_bar.add(this.m_plot_line);
    }

    private void createJMenuBarChart() {
        //creation of bar

        this.m_plot_bar = new JMenu("Bar");
        this.m_bar_col = new JMenuItem("Col");
        this.m_bar_col.addActionListener(this);
        this.m_plot_bar.add(this.m_bar_col);
        this.m_bar_row = new JMenuItem("Row");
        this.m_bar_row.addActionListener(this);
        this.m_plot_bar.add(this.m_bar_row);
        this.m_bar_dataset = new JMenuItem("Dataset");
        this.m_bar_dataset.addActionListener(this);
        this.m_plot_bar.add(this.m_bar_dataset);

        this.m_plot_line_bar.add(m_plot_bar);

        this.m_menu_plot.add(m_plot_line_bar);

        this.m_menu_bar.add(this.m_menu_plot);
    }
    
    private void createJMenuMacros() {
        this.m_macros = new JMenu("Macros");
        
        this.m_macros_define = new JMenuItem("Define a macro");
        this.m_macros_define.addActionListener(this);
        
        this.m_macros_load = new JMenuItem("Load a macro");
        
        this.m_macros_execute = new JMenuItem("Execute a macro");
        
        this.m_macros.add(this.m_macros_define);
        this.m_macros.add(this.m_macros_load);
        this.m_macros.add(this.m_macros_execute);
        
        this.m_menu_bar.add(this.m_macros);
    }

    private void createScrollPaneDataset(DatasetDTO dto) {
        //DatasetDTO dto = new DatasetDTO(5,5,"datasetTest");
        List<List<Integer>> dataset_info = dto.getData();

        int rows = dataset_info.size();
        int cols = dataset_info.get(0).size();

        String[] columns = new String[cols];
        Object[][] data = new Object[dataset_info.size()][];

        int k = 0;
        for (List<Integer> line : dataset_info) {
            data[k] = line.toArray();
            k++;
        }
        for (int j = 0; j < cols; j++) {
            columns[j] = Character.toString((char) (j + 65));
        }
        MyTableModel model = new MyTableModel(columns, data);
        //JTable table = new JTable(model);

        //Método para fazer repaint do header das colunas para seleccionar a coluna que está atualmente a ser alterada.
        JTable table = new MyJTable(model);
        RowNumberTable rowTable = new RowNumberTable(table);
        //add table to list of tables
        this.m_datasets.add(table);
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        //this.m_scroll_panes_dataset.add(scrollPane);
        this.addJScrollPaneToJTabbedPane(scrollPane, dto.getName());
        scrollPane.setRowHeaderView(rowTable);
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER,
                rowTable.getTableHeader());

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null && arg instanceof DatasetDTO) {
            this.addNewDataset((DatasetDTO) arg);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == this.m_dataset_enter) {
            m_controller.userChoosedEnterDataset();
        } else if (e.getSource() == this.m_dataset_save) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedSaveDataset(selected);
        } else if (e.getSource() == this.m_dataset_open) {
            this.m_controller.userChoosedOpenExistingDataset();
        } else if (e.getSource() == this.m_calculate_rows_total) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int row = this.m_datasets.get(selected).getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedCalculateRowsTotal(row);
        } else if (e.getSource() == this.m_calculate_columns_total) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int col = this.m_datasets.get(selected).getSelectedColumn();
            if (col == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedCalculateColumnsTotal(col);
        } else if (e.getSource() == this.m_show_dataset) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedShowNumberOfElements(TYPE.DATASET);
        } else if (e.getSource() == this.m_show_selected_row) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int row = this.m_datasets.get(selected).getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedShowNumberOfElements(TYPE.ROW);
        } else if (e.getSource() == this.m_show_selected_column) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int col = this.m_datasets.get(selected).getSelectedColumn();
            if (col == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedShowNumberOfElements(TYPE.COLUMN);
        } else if (e.getSource() == this.m_file_export) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedExportDataset(selected);
        } else if (e.getSource() == this.m_file_import) {
            this.m_controller.userChoosedImportDataset();
        } else if (e.getSource() == this.m_statistical_measures_dataset
                || e.getSource() == this.m_statistical_measures_selected_row
                || e.getSource() == this.m_statistical_measures_selected_column) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (e.getSource() == this.m_statistical_measures_dataset) {
                this.m_controller.userChoosedStatisticalMeasure(TYPE.DATASET);
            } else if (e.getSource() == this.m_statistical_measures_selected_row) {
                int row = this.m_datasets.get(selected).getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                this.m_controller.userChoosedStatisticalMeasure(TYPE.ROW);
            } else if (e.getSource() == this.m_statistical_measures_selected_column) {
                int col = this.m_datasets.get(selected).getSelectedColumn();
                if (col == -1) {
                    JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                this.m_controller.userChoosedStatisticalMeasure(TYPE.COLUMN);
            }
        } else if (e.getSource() == this.m_menu_transformation) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedTransformations(selected);
        } else if (e.getSource() == this.m_pie_row) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int row = this.m_datasets.get(selected).getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedPieChart(selected, TYPE.ROW);
        } else if (e.getSource() == this.m_pie_col) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int col = this.m_datasets.get(selected).getSelectedColumn();
            if (col == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedPieChart(selected, TYPE.COLUMN);
        } else if (e.getSource() == this.m_line_col) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int col = this.m_datasets.get(selected).getSelectedColumn();
            if (col == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedLineChart(selected, TYPE.COLUMN);
        } else if (e.getSource() == m_line_row) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int row = this.m_datasets.get(selected).getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedLineChart(selected, TYPE.ROW);
        } else if (e.getSource() == m_line_dataset) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedLineChart(selected, TYPE.DATASET);
        } else if (e.getSource() == m_bar_col) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int col = this.m_datasets.get(selected).getSelectedColumn();
            if (col == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a column", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedBarChar(selected,TYPE.COLUMN);
        } else if (e.getSource() == m_bar_row) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int row = this.m_datasets.get(selected).getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(this.window, "Select a row", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
             this.m_controller.userChoosedBarChar(selected,TYPE.ROW);
        } else if (e.getSource() == m_bar_dataset) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedBarChar(selected,TYPE.DATASET);
        }  else if (e.getSource() == this.m_macros_define) {
            int selected = this.m_datasets_container.getSelectedIndex();
            if (selected == -1) {
                JOptionPane.showMessageDialog(this.window, "There are no datasets yet.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.m_controller.userChoosedDefineMacro(selected);
        }
    }

    public EnterDatasetController showEnterDatasetView() {

        EnterDatasetModel model = new EnterDatasetModel();
        EnterDatasetController controller = new EnterDatasetController(window, model);
        return controller;

    }

    private void addNewDataset(DatasetDTO datasetDTO) {
        createScrollPaneDataset(datasetDTO);
    }

    private void createJTabbedPane() {
        this.m_datasets_container = new JTabbedPane();

    }

    private void addJScrollPaneToJTabbedPane(JScrollPane pane, String dataset_name) {
        this.m_datasets_container.addTab(this.count + "", null, pane, dataset_name);
        this.count++;
        this.window.pack();
        this.window.revalidate();
    }

    public List<List<Integer>> getDataset(int selected) throws NumberFormatException {

        MyTableModel model = (MyTableModel) this.m_datasets.get(selected).getModel();
        Object[][] data = model.getData();
        int rows = model.getRowCount();
        int cols = model.getColumnCount();

        List<List<Integer>> datasetInteger = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            List<Integer> line = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                line.add((Integer) data[i][j]);
            }
            datasetInteger.add(line);
        }
        return datasetInteger;
    }

    public void showErrorMessage(String error_msg) {
        JOptionPane.showMessageDialog(this.window, error_msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void thereAreNoDatasetsAvailable()
    {
            JOptionPane.showMessageDialog(window, "There are no datasets", APPLICATION_NAME, JOptionPane.ERROR_MESSAGE);
    }
    public DatasetDTO showDatasets(List<DatasetDTO> existingDatasets) {


        
        Object[] array = existingDatasets.toArray();
        DatasetDTO dto = (DatasetDTO) JOptionPane.showInputDialog(this.window,
                "Which dataset do you want to open?",
                "Open Dataset",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return dto;
    }

    public String showExportTypes(List<String> exportTypes) {

        Object[] array = exportTypes.toArray();
        String choosenExportType = (String) JOptionPane.showInputDialog(this.window,
                "Choose the desired file type",
                "Export dataset",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return choosenExportType;
    }

    public String showTransformationTypes(List<String> transformationTypes) {

        Object[] array = transformationTypes.toArray();
        String choosenTransformationType = (String) JOptionPane.showInputDialog(this.window,
                "Choose the desired transformation",
                "Apply a transformation",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return choosenTransformationType;
    }

    public int askTransformationScaleParameters() {

        int value = 0;

        try {
            value = Integer.parseInt(JOptionPane.showInputDialog(this.window,
                    "Enter the scalar value",
                    "Transformation values",
                    JOptionPane.QUESTION_MESSAGE));
        } catch (NumberFormatException ex) {
            Logger.getLogger(DatasetView.class.getName()).log(Level.SEVERE, null, ex);
            this.showErrorMessage("Enter a value numerical value!");
        }

        return value;
    }
    
    public DatasetDTO askTransformationDatasetParameters() {
        
        List<DatasetDTO> datasetDTOs = new ArrayList<>();
        for (int i = 0; i < this.count; i++) {
            List<List<Integer>> datasetList = this.getDataset(i);
            this.m_model.updateDataset(datasetList, i);
            DatasetDTO dto = this.m_model.getDatasetDTO(i);
            datasetDTOs.add(dto);
        }
        
        Object[] array = datasetDTOs.toArray();
        DatasetDTO choosenDataset = (DatasetDTO) JOptionPane.showInputDialog(this.window,
                "Choose the desired dataset",
                "Apply a transformation",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return choosenDataset;
    }
    
    public File askForImportFile() {
        File workingDirectory = new File(System.getProperty("user.dir"));
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(workingDirectory);
        int returnValue = fileChooser.showOpenDialog(this.window);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        }
        return null;
    }
    
    public String showLinearInterpolationTypes(List<String> linearInterpolationTypes) {

        Object[] array = linearInterpolationTypes.toArray();
        String choosenLIType = (String) JOptionPane.showInputDialog(this.window,
                "Choose the desired linear interpolation type",
                "Apply a transformation",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return choosenLIType;
    }

    public int getSelectedDatasetNumber() {
        return this.m_datasets_container.getSelectedIndex();
    }

    public void showTotal(int total) {
        JOptionPane.showMessageDialog(this.window, "The total is " + total);
    }

    public void showNumberOfElements(int number) {
        JOptionPane.showMessageDialog(this.window, "The number of elements is " + number);
    }

    public int getSelectedColumn(int selected) {
        JTable table = this.m_datasets.get(selected);
        return table.getSelectedColumn();
    }

    public int getSelectedRow(int selected) {
        JTable table = this.m_datasets.get(selected);
        return table.getSelectedRow();
    }

    public String showStatisticalMeasures(List<String> available) {
        Object[] array = available.toArray();
        String choosenStatisticalMeasure = (String) JOptionPane.showInputDialog(this.window,
                "Choose the desired statistical measure",
                "Statistical Measure",
                JOptionPane.QUESTION_MESSAGE,
                null,
                array,
                array[0]);
        return choosenStatisticalMeasure;
    }

    public void showStatisticalMeasureResult(double result) {
        JOptionPane.showMessageDialog(this.window, "The result is " + ((double) Math.round(result * 100) / 100));
    }

    public void pieChart(List<Integer> col_or_row) {
        PieChart pie = new PieChart(window, APPLICATION_NAME, true, col_or_row);
    }

    public void barChart(List<Integer> col_or_row) {
        BarChart bar = new BarChart(window, APPLICATION_NAME, true, col_or_row);
    }

    public void lineChart(List<Integer> col_or_row) {
        LineChart line = new LineChart(window, APPLICATION_NAME, true, col_or_row);
    }
    
    public boolean askForMacroConfirmation() {
        int opt = JOptionPane.showConfirmDialog(this.window, "Do you want to add more transformations to this macro?", 
                "Define a macro", JOptionPane.YES_NO_OPTION);
        
        return opt == JOptionPane.YES_OPTION;
    }
}
