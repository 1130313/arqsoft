/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view.components;

import javax.swing.JTextField;

/**
 *
 * @author Outros
 */
public class MyJTextField extends JTextField{

    private int row;
    private int col;
    
    public MyJTextField(String text, int columns, int row, int col) {
        super(text, columns);
        this.row = row;
        this.col = col;
    }

    /**
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * @return the col
     */
    public int getCol() {
        return col;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * @param col the col to set
     */
    public void setCol(int col) {
        this.col = col;
    }
    
    
    
    
}
