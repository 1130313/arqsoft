/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view.components;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;



//source : http://stackoverflow.com/questions/15489818/highlighting-a-column-header-of-a-jtable
/**
 *
 * @author Outros
 */
public class ColumnHeaderRenderer extends DefaultTableHeaderCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        int selectedColumn = table.getSelectedColumn();
        if (selectedColumn == column) {
            Color bg = table.getSelectionBackground();
            setBackground(bg);
            setOpaque(true);
        } else {
            setOpaque(false);
        }

        return this;
    }

}
