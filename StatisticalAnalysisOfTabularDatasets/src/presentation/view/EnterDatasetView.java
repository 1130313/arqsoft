/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view;

import DTO.DatasetDTO;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import presentation.controller.DatasetController;
import presentation.controller.EnterDatasetController;
import presentation.model.DatasetModel;
import presentation.model.EnterDatasetModel;
import presentation.view.components.MyJTable;
import presentation.view.components.MyTableModel;
import presentation.view.components.RowNumberTable;

/**
 *
 * @author Outros
 */
public class EnterDatasetView implements Observer, ActionListener {

    private final String WINDOW_NAME = "Enter a dataset";

    private String name;
    private int rows;
    private int cols;
    private JPanel m_buttons_panel;
    private JButton m_button_exit;
    private JButton m_button_enter;

    private JScrollPane m_scroll_pane_dataset;
    private JTable m_dataset_table;

    private JDialog newDialog;

    private EnterDatasetController m_controller;
    private EnterDatasetModel m_model;

    public EnterDatasetView(EnterDatasetController controller, EnterDatasetModel model) {

        this.m_controller = controller;
        this.m_model = model;
        this.m_model.addObserver((Observer) this);
    }

    public void createView(JFrame main_window) {
        newDialog = new JDialog(main_window, WINDOW_NAME, true);

        if (main_window != null) {
            Dimension parentSize = main_window.getSize();
            Point p = main_window.getLocation();
            newDialog.setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
        }
        newDialog.setLayout(new BorderLayout());
        newDialog.add(m_scroll_pane_dataset, BorderLayout.CENTER);
        newDialog.add(m_buttons_panel, BorderLayout.SOUTH);
        newDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        newDialog.pack();
        newDialog.setVisible(true);
    }

    public void createControlls() {
        createScrollPaneDataset();
        createOptions();
    }

    private void createOptions() {
        this.m_buttons_panel = new JPanel(new FlowLayout());
        this.m_button_exit = new JButton("Exit");
        this.m_button_exit.addActionListener(this);
        this.m_button_enter = new JButton("Enter");
        this.m_button_enter.addActionListener(this);

        this.m_buttons_panel.add(m_button_enter);
        this.m_buttons_panel.add(m_button_exit);

    }

    public void createScrollPaneDataset() {

        int rows = this.rows;
        int cols = this.cols;

        String[] columns = new String[cols];
        Object[][] data = new Object[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                data[i][j] = new Integer(0);
            }
        }
        for (int j = 0; j < cols; j++) {
            columns[j] = Character.toString((char) (j + 65));
        }
        MyTableModel model = new MyTableModel(columns, data);
        //JTable table = new JTable(model);

        //Método para fazer repaint do header das colunas para seleccionar a coluna que está atualmente a ser alterada.
        this.m_dataset_table = new MyJTable(model);
        RowNumberTable rowTable = new RowNumberTable(this.m_dataset_table);
        //Create the scroll pane and add the table to it.
        this.m_scroll_pane_dataset = new JScrollPane(this.m_dataset_table);
        this.m_scroll_pane_dataset.setRowHeaderView(rowTable);
        this.m_scroll_pane_dataset.setCorner(JScrollPane.UPPER_LEFT_CORNER,
                rowTable.getTableHeader());
    }

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.m_button_enter) {
            this.m_controller.userPressedEnter();
        } else if (e.getSource() == this.m_button_exit) {
            this.m_controller.userPressedExit();
        }
    }

    public boolean isNumber(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException exc) {
            return false;
        }
    }

    public void askForRowsNumber() {

        String input = JOptionPane.showInputDialog("Insert rows number");
        while (!isNumber(input)) {
            input = JOptionPane.showInputDialog("Insert rows number");
        }
        this.rows = Integer.parseInt(input);
    }

    public void askForColumnsNumber() {
        String input = JOptionPane.showInputDialog("Insert cols number");
        while (!isNumber(input)) {
            input = JOptionPane.showInputDialog("Insert cols number");
        }
        this.cols = Integer.parseInt(input);
    }

    public int getRows() {
        return this.rows;
    }

    public int getColumns() {
        return this.cols;
    }

    public String askForDatasetName() {
        String input = JOptionPane.showInputDialog("Insert dataset's name");
        this.name = input;
        return input;
    }

    public void dispose() {
        this.newDialog.setVisible(false);
        this.newDialog.dispose();

    }

    public List<List<Integer>> getDataset() throws NumberFormatException {
        MyTableModel model = (MyTableModel) this.m_dataset_table.getModel();
        Object[][] data = model.getData();
        int rows = model.getRowCount();
        int cols = model.getColumnCount();

        List<List<Integer>> datasetInteger = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            List<Integer> line = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                line.add((Integer) data[i][j]);
            }
            datasetInteger.add(line);
        }
        return datasetInteger;
    }

    public void showErrorMessage(String error_msg) {
        JOptionPane.showMessageDialog(this.newDialog, error_msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
