/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

public class BarChart extends JDialog
{
        private Map<Integer, Integer> countMap = new HashMap<>();
   List<Integer> data;

   public BarChart( JFrame parent, String applicationTitle, boolean modal, List<Integer> data )
   {
      super( parent, applicationTitle, modal);
      this.data = new ArrayList<>();
      this.data.addAll(data);
      JFreeChart barChart = ChartFactory.createBarChart(
         "Bar Chart",
         "Values",
         "Occurrences",
         createDataset(),
         PlotOrientation.VERTICAL,
         true, true, false);

      ChartPanel chartPanel = new ChartPanel( barChart );
      chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );
      setContentPane( chartPanel );
      this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      this.pack( );
      RefineryUtilities.centerFrameOnScreen(this);
      this.setLocationRelativeTo(null);
      this.setVisible(true);
   }
       private void findCount() {
        for (Integer value : data) {
            int count = 0;

            if (countMap.containsKey(value)) {
                count = countMap.get(value) + 1;
            } else {
                count = 1;
            }
            countMap.put(value, count);
        }
    }


   private CategoryDataset createDataset( )
   {
      final DefaultCategoryDataset dataset =
      new DefaultCategoryDataset( );
        findCount();
        for (final Map.Entry<Integer, Integer> tuple : countMap.entrySet()) {

            Integer value = tuple.getKey();
            Integer count = tuple.getValue();
            dataset.addValue(count.doubleValue(), Integer.toString(value), Integer.toString(value));
        }
      return dataset;
   }
}