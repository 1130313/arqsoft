/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.view;

/**
 *
 * @author Outros
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class PieChart extends JDialog///ApplicationFrame 
{
    private List<Integer> data = new ArrayList<>();
    private Map<Integer, Integer> countMap = new HashMap<>();
    
    public PieChart(JFrame parent, String title, boolean modal, List<Integer> data) {
        super(parent, title, modal);
        this.data.addAll(data);
        setContentPane(createDemoPanel());
        setSize(560, 367);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    private void findCount()
    {
        for (Integer value : data) {
            int count = 0;

            if (countMap.containsKey(value)) {
                count = countMap.get(value) + 1;
            } else {
                count = 1;
            }
            countMap.put(value, count);
        }
    }
    private PieDataset createDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        findCount();
        for (final Map.Entry<Integer, Integer> tuple : countMap.entrySet()) {
            
            Integer value = tuple.getKey();
            Integer count = tuple.getValue();
            dataset.setValue(Integer.toString(value), new Double(count));
        }
        return dataset;
    }

    private JFreeChart createChart(PieDataset dataset) {
        JFreeChart chart = ChartFactory.createPieChart(
                "Pie Chart", // chart title 
                dataset, // data    
                true, // include legend   
                true,
                false);

        return chart;
    }

    public JPanel createDemoPanel() {
        JFreeChart chart = createChart(createDataset());
        return new ChartPanel(chart);
    }

//    public static void main(String[] args) {
//        PieChart demo = new PieChart("Mobile Sales");
//        demo.setSize(560, 367);
//        RefineryUtilities.centerFrameOnScreen(demo);
//        demo.setVisible(true);
//    }
}
