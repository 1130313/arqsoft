/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.model;

import DTO.DatasetDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JTextField;

/**
 *
 * @author Outros
 */
public class DatasetModel extends Observable {
    
    List<DatasetDTO> datasets;
    
    public DatasetModel()
    {
        datasets = new ArrayList<>();
        
    }
    public void addDatasetDTO(DatasetDTO dto) {
        datasets.add(dto);
        this.setChanged();
        this.notifyObservers(dto);
    }
    
    public void updateDataset(List<List<Integer>> dataset, int selected) {
        DatasetDTO dto = datasets.get(selected);
        dto.fillData(dataset);
    }
    public DatasetDTO getDatasetDTO(int index){
        return datasets.get(index);
    }

    public void updateDataset(Long newId, int selected) {
        DatasetDTO dto = datasets.get(selected);
        dto.setId(newId);
    }
}
