/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.model;

import DTO.DatasetDTO;
import java.util.List;
import java.util.Observable;
import javax.swing.JTextField;

/**
 *
 * @author Outros
 */
public class EnterDatasetModel extends Observable {
    
    DatasetDTO dto ;
    
    public EnterDatasetModel(){ 
        
    }
    public void initialize(int rows, int cols, String name){
        this.dto = new DatasetDTO(rows,cols,name);
    }
    public void fillDTO(List<List<Integer>> entered_dataset){
        this.dto.fillData(entered_dataset);
    }
    public DatasetDTO getDto()
    {
        return this.dto;
    }
}
