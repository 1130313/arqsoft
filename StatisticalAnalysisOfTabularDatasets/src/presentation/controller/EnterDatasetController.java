/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.controller;


import DTO.DatasetDTO;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JTextField;
import presentation.model.EnterDatasetModel;
import presentation.view.EnterDatasetView;

/**
 *
 * @author Outros
 */
public class EnterDatasetController {
 
    private EnterDatasetView m_view;
    private EnterDatasetModel m_model;
    private boolean resultValue;
    
    public EnterDatasetController(JFrame main_window, EnterDatasetModel model )
    {
        this.m_model = model;
        this.resultValue = false;
        m_view = new EnterDatasetView(this,model);
        String name = m_view.askForDatasetName();
        m_view.askForRowsNumber();
        m_view.askForColumnsNumber();
        this.m_model.initialize(m_view.getRows(),m_view.getColumns(),name);
        m_view.createControlls();
        m_view.createView(main_window);
        
    }

    public void userPressedEnter() {
        try
        {
        List<List<Integer>> dataset = this.m_view.getDataset();
        m_model.fillDTO(dataset);
        m_view.dispose();
        resultValue = true;
        }
        catch(NumberFormatException ex)
        {
           this.m_view.showErrorMessage("Please, just numeric values on the dataset!");
        }
    }

    public void userPressedExit() {
        m_view.dispose();
        resultValue = false;
    }
    
    public boolean isResultValue()
    {
        return resultValue;
    }
    public DatasetDTO getDatasetDTO(){
        
        return this.m_model.getDto();
    }
}
