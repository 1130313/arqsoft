/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.controller;

import DTO.DatasetDTO;
import DTO.DatasetDTOCount;
import Model.Dataset;
import bl.BLController;
import bl.BLController.TYPE;
import java.io.File;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import presentation.model.DatasetModel;
import presentation.view.DatasetView;

/**
 *
 * @author Outros
 */
public class DatasetController {

    DatasetModel m_dataset_model;
    DatasetView m_dataset_view;

    public DatasetController(DatasetModel dataset_model) {
        this.m_dataset_model = dataset_model;
        this.m_dataset_view = new DatasetView(this, dataset_model);
        this.m_dataset_view.createControls();
        this.m_dataset_view.createView();

    }

    public void userChoosedEnterDataset() {
        EnterDatasetController controller = this.m_dataset_view.showEnterDatasetView();
        if (controller.isResultValue()) {
            DatasetDTO dto = controller.getDatasetDTO();
            this.m_dataset_model.addDatasetDTO(dto);
        }
    }

    public void userChoosedSaveDataset(int selected) {
        try {
            List<List<Integer>> dataset = this.m_dataset_view.getDataset(selected);
            this.m_dataset_model.updateDataset(dataset, selected);
            DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selected);
            BLController cont = new BLController();
            Long newId = cont.saveDataset(dto);
            this.m_dataset_model.updateDataset(newId, selected);
        } catch (NumberFormatException ex) {
            this.m_dataset_view.showErrorMessage("Please, just numeric values on the dataset!");
        }
    }

    public void userChoosedOpenExistingDataset() {
        BLController controller = new BLController();
        List<DatasetDTO> existingDatasets = controller.getAvailableDatasets();
        if (existingDatasets.isEmpty()) {
            this.m_dataset_view.thereAreNoDatasetsAvailable();
        } else {
            DatasetDTO choosedDTO = this.m_dataset_view.showDatasets(existingDatasets);
            if (choosedDTO != null) {
                this.m_dataset_model.addDatasetDTO(choosedDTO);
            }
        }
    }

    public void userChoosedCalculateRowsTotal(int choosedRow) {
        int selectedDatasetNumber = this.m_dataset_view.getSelectedDatasetNumber();
        if (choosedRow != -1) {
            this.m_dataset_model.updateDataset(this.m_dataset_view.getDataset(selectedDatasetNumber), selectedDatasetNumber);
            DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selectedDatasetNumber);
            BLController controller = new BLController();
            int total = controller.calculateRowsTotal(dto, choosedRow);
            this.m_dataset_view.showTotal(total);
        }
    }

    public void userChoosedCalculateColumnsTotal(int choosedColumn) {
        if (choosedColumn != -1) {
            int selectedDatasetNumber = this.m_dataset_view.getSelectedDatasetNumber();
            this.m_dataset_model.updateDataset(this.m_dataset_view.getDataset(selectedDatasetNumber), selectedDatasetNumber);
            DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selectedDatasetNumber);
            BLController controller = new BLController();
            int total = controller.calculateColumnsTotal(dto, choosedColumn);
            this.m_dataset_view.showTotal(total);
        }
    }

    public void userChoosedShowNumberOfElements(TYPE type) {
        int selected = this.m_dataset_view.getSelectedDatasetNumber();
        List<List<Integer>> dtset = this.m_dataset_view.getDataset(selected);
        this.m_dataset_model.updateDataset(dtset, selected);
        DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selected);
        BLController controller = new BLController();
        DatasetDTOCount dtoWithColRowNumber = new DatasetDTOCount(dto);

        switch (type) {
            case COLUMN: {
                int col = this.m_dataset_view.getSelectedColumn(selected);
                dtoWithColRowNumber.setColNumber(col);
                break;
            }
            case ROW: {
                int row = this.m_dataset_view.getSelectedRow(selected);
                dtoWithColRowNumber.setRowNumber(row);

            }
        }
        int numberOfElements = controller.numberOfElements(type, dtoWithColRowNumber);
        this.m_dataset_view.showNumberOfElements(numberOfElements);
    }

    public void userChoosedExportDataset(int selectedDataset) {
        BLController controller = new BLController();
        List<String> exportTypes = controller.getAvailableExportTypes();
        String choosenExportType = this.m_dataset_view.showExportTypes(exportTypes);
        if (choosenExportType != null) {
            DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
            controller.exportDataset(choosenExportType, dto);
        }
    }

    public void userChoosedImportDataset() {
        File choosenFileToImport = this.m_dataset_view.askForImportFile();
        if (choosenFileToImport != null) {
            try {
                BLController controller = new BLController();
                DatasetDTO importedDatasetDTO = controller.importDataset(choosenFileToImport);
                if (importedDatasetDTO != null) {
                    this.m_dataset_model.addDatasetDTO(importedDatasetDTO);
                }
            } catch (Exception ex) {
                Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                this.m_dataset_view.showErrorMessage(ex.getMessage());
            }
        }
    }

    public void userChoosedStatisticalMeasure(TYPE type) {

        BLController controller = new BLController();
        List<String> available = controller.getAvailableStatisticalMeasures();

        String choosenStatisticalMeasure = this.m_dataset_view.showStatisticalMeasures(available);
        if (choosenStatisticalMeasure == null) {
            return;
        }
        int selected = this.m_dataset_view.getSelectedDatasetNumber();
        List<List<Integer>> dtset = this.m_dataset_view.getDataset(selected);
        this.m_dataset_model.updateDataset(dtset, selected);
        DatasetDTO dto = this.m_dataset_model.getDatasetDTO(selected);

        DatasetDTOCount dtoWithColRowNumber = new DatasetDTOCount(dto);

        switch (type) {
            case COLUMN: {
                int col = this.m_dataset_view.getSelectedColumn(selected);
                dtoWithColRowNumber.setColNumber(col);
                break;
            }
            case ROW: {
                int row = this.m_dataset_view.getSelectedRow(selected);
                dtoWithColRowNumber.setRowNumber(row);

            }
        }
        double result = controller.calculateStatisticalMeasure(type, dtoWithColRowNumber, choosenStatisticalMeasure);
        this.m_dataset_view.showStatisticalMeasureResult(result);
    }

    public void userChoosedTransformations(int selectedDataset) {
        BLController controller = new BLController();
        List<String> transformations = controller.getAvailableTransformations();
        String transformationType = this.m_dataset_view.showTransformationTypes(transformations);

        DatasetDTO dto;
        DatasetDTO newDatasetDTO;
        if (transformationType != null) {
            switch (transformationType) {
                case "Transpose":
                    try {
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, null);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
                case "Scale":
                    try {
                        int mulScalar = this.m_dataset_view.askTransformationScaleParameters();
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, mulScalar);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
                case "Add":
                    try {
                        int addScalar = this.m_dataset_view.askTransformationScaleParameters();
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, addScalar);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
                case "AddDataset":
                    try {
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        DatasetDTO secondDatasetDTO = this.m_dataset_view.askTransformationDatasetParameters();
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, secondDatasetDTO);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
                case "MultiplyDataset":
                    try {
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        DatasetDTO secondDatasetDTO = this.m_dataset_view.askTransformationDatasetParameters();
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, secondDatasetDTO);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
                case "LinearInterpolation":
                    try {
                        List<String> linearInterpolationTypes = controller.getAvailableLinearInterpolationTypes();
                        String choosenLIType = this.m_dataset_view.showLinearInterpolationTypes(linearInterpolationTypes);
                        dto = this.m_dataset_model.getDatasetDTO(selectedDataset);
                        newDatasetDTO = controller.executeTransformation(transformationType, dto, choosenLIType);
                        this.m_dataset_model.addDatasetDTO(newDatasetDTO);
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(DatasetController.class.getName()).log(Level.SEVERE, null, ex);
                        this.m_dataset_view.showErrorMessage(ex.getMessage());
                    }
                    break;
            }
        }
    }

    public void userChoosedPieChart(int selected, TYPE type) {
        List<List<Integer>> dtset = this.m_dataset_view.getDataset(selected);
        List<Integer> col_or_row = new ArrayList<>();
        int rows = dtset.size();
        int cols = dtset.get(0).size();
        switch (type) {
            case COLUMN: {
                int col = this.m_dataset_view.getSelectedColumn(selected);
                for (int i = 0; i < rows; i++) {
                    col_or_row.add(new Integer(dtset.get(i).get(col)));
                }
                break;
            }
            case ROW: {
                int row = this.m_dataset_view.getSelectedRow(selected);
                for (int i = 0; i < cols; i++) {
                    col_or_row.add(new Integer(dtset.get(row).get(i)));
                }
                break;
            }
        }
        m_dataset_view.pieChart(col_or_row);

    }

    public void userChoosedLineChart(int selected, TYPE type) {
        List<List<Integer>> dtset = this.m_dataset_view.getDataset(selected);
        List<Integer> dataArray = new ArrayList<>();
        int rows = dtset.size();
        int cols = dtset.get(0).size();
        switch (type) {
            case DATASET: {
                for (List<Integer> line : dtset) {
                    dataArray.addAll(line);
                }
                break;
            }
            case COLUMN: {
                int col = this.m_dataset_view.getSelectedColumn(selected);
                for (int i = 0; i < rows; i++) {
                    dataArray.add(new Integer(dtset.get(i).get(col)));
                }
                break;
            }
            case ROW: {
                int row = this.m_dataset_view.getSelectedRow(selected);
                for (int i = 0; i < cols; i++) {
                    dataArray.add(new Integer(dtset.get(row).get(i)));
                }
                break;
            }
        }
        m_dataset_view.lineChart(dataArray);
    }

    public void userChoosedBarChar(int selected, TYPE type) {
        List<List<Integer>> dtset = this.m_dataset_view.getDataset(selected);
        List<Integer> dataArray = new ArrayList<>();
        int rows = dtset.size();
        int cols = dtset.get(0).size();
        switch (type) {
            case DATASET: {
                for (List<Integer> line : dtset) {
                    dataArray.addAll(line);
                }
                break;
            }
            case COLUMN: {
                int col = this.m_dataset_view.getSelectedColumn(selected);
                for (int i = 0; i < rows; i++) {
                    dataArray.add(new Integer(dtset.get(i).get(col)));
                }
                break;
            }
            case ROW: {
                int row = this.m_dataset_view.getSelectedRow(selected);
                for (int i = 0; i < cols; i++) {
                    dataArray.add(new Integer(dtset.get(row).get(i)));
                }
                break;
            }
        }
        m_dataset_view.barChart(dataArray);
    }

    public void userChoosedDefineMacro(int selectedDataset) {
        BLController controller = new BLController();
        List<String> transformations = controller.getAvailableTransformations();
        List<Entry<String, Object>> macroData = new ArrayList<>();
        Entry<String, Object> newPair;

        newPair = this.promptMacroTransformations(transformations);
        if (newPair != null) {
            macroData.add(newPair);
        }

        while (this.m_dataset_view.askForMacroConfirmation()) {
            newPair = this.promptMacroTransformations(transformations);
            if (newPair != null) {
                macroData.add(newPair);
            }
        }

        DatasetDTO datasetDTO = this.m_dataset_model.getDatasetDTO(selectedDataset);
        controller.defineMacro(macroData, datasetDTO);
    }

    private Entry<String, Object> promptMacroTransformations(List<String> transformationTypes) {

        String transformationType = this.m_dataset_view.showTransformationTypes(transformationTypes);
        if (transformationType != null) {
            Entry<String, Object> newPair;
            switch (transformationType) {
                case "Transpose":
                    newPair = new SimpleEntry<>(transformationType, null);
                    return newPair;
                case "Scale":
                    int mulScalar = this.m_dataset_view.askTransformationScaleParameters();
                    newPair = new SimpleEntry<>(transformationType, mulScalar);
                    return newPair;
                case "Add":
                    int addScalar = this.m_dataset_view.askTransformationScaleParameters();
                    newPair = new SimpleEntry<>(transformationType, addScalar);
                    return newPair;
                case "AddDataset":
                case "MultiplyDataset":
                    DatasetDTO secondDatasetDTO = this.m_dataset_view.askTransformationDatasetParameters();
                    newPair = new SimpleEntry<>(transformationType, secondDatasetDTO);
                    return newPair;
                case "LinearInterpolation":
                    BLController controller = new BLController();
                    List<String> linearInterpolationTypes = controller.getAvailableLinearInterpolationTypes();
                    String choosenLIType = this.m_dataset_view.showLinearInterpolationTypes(linearInterpolationTypes);
                    newPair = new SimpleEntry<>(transformationType, choosenLIType);
                    return newPair;
            }
        }

        return null;
    }
}
