/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.swing.JTextField;
import presentation.view.DatasetView;

/**
 *
 * @author vitormoreira
 */
@Entity
@Table(name = "DATASET")
@SequenceGenerator(name = "DATASET_SEQUENCE", sequenceName = "DATASET_SEQUENCE", allocationSize = 1, initialValue = 0)
public class Dataset implements Serializable, Transformable, Measurable,Countable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DATASET_SEQUENCE")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "DATASET_ID")
    private List<Row> rows;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "DATASET_ID")
    private List<Column> columns;
    
    private String name;

    public Dataset() {

    }

    public Dataset(int rows, int cols) {

        this.rows = new ArrayList<>();
        this.columns = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            Row r = new Row();
            this.rows.add(r);
        }
        for (int i = 0; i < cols; i++) {
            Column c = new Column();
            this.columns.add(c);
        }

        //Array para guardar um array de colunas //cada coluna é um array de celulas
        ArrayList<ArrayList<Cell>> columns_arrays = new ArrayList<>();
        for (int i = 0; i < cols; i++) {
            ArrayList<Cell> newColumn = new ArrayList<>();
            columns_arrays.add(newColumn);
        }

        //Geração de cada celula
        for (int i = 0; i < this.rows.size(); i++) {
            //Criação de um array para row i
            ArrayList<Cell> row_cells = new ArrayList<>();
            for (int j = 0; j < this.columns.size(); j++) {
                Cell newCell = new Cell();
                row_cells.add(newCell);
                columns_arrays.get(j).add(newCell);
            }
            this.rows.get(i).setCells(row_cells);
        }
        for (int i = 0; i < columns_arrays.size(); i++) {
            this.columns.get(i).setCells(columns_arrays.get(i));
        }
    }

//    public Dataset(ArrayList<Row> rows, ArrayList<Column> columns) {
//        this.rows = rows;
//        this.columns = columns;
//    }
    public List<Row> getRows() {
        return rows;
    }

    public List<Column> getColumns() {
        return columns;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getRowsSize() {
        return this.rows.size();
    }
    
    public int getColumnsSize() {
        return this.columns.size();
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void fillByRows(ArrayList<Row> rows) {
        int i = 0;
        for (Row row : rows) {
            int j = 0;
            
            // data used to fill
            List<Cell> cells = row.getCells();
            
            // data to be filled
            Row currentRowToBeFilled = this.rows.get(i);
            List<Cell> cellsFromCurrentRow = currentRowToBeFilled.getCells();
            
            for (Cell cellToBeFilled : cellsFromCurrentRow) {
                cellToBeFilled.setValue(cells.get(j).getValue());
                j++;
            }
            i++;
        }
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dataset)) {
            return false;
        }
        Dataset other = (Dataset) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Test[ id=" + id + " ]";
    }
    public void fillDataset(List<List<Integer>> data) {

        for (int i = 0; i < this.rows.size(); i++) {
            List<Cell> cells = this.rows.get(i).getCells();
            int j = 0;
            for (Cell cell : cells) {
                cell.setValue(data.get(i).get(j));
                j++;
            }
        }
    }
    public List<List<Integer>> getData(){
        
        List<List<Integer>> data = new ArrayList<>();
        
        for (Row row : rows) {
            List<Cell> cells = row.getCells();
            List<Integer> values = new ArrayList<>();
            for (Cell c : cells) {
                int value = c.getValue();
                values.add(new Integer(value));
            }
            data.add(values);
        }
        return data;
    }

    @Override
    public int countNumberOfElements() {
        int number = 0;
        for (Row row : rows) {
            number+=row.countNumberOfElements();
        }
        return number;
    }

}
