/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author vitormoreira
 */
@Entity
@Table(name = "CELL")
@SequenceGenerator(name = "CELL_SEQUENCE", sequenceName = "CELL_SEQUENCE", allocationSize = 1, initialValue = 0)
public class Cell implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "CELL_SEQUENCE")
    public Long id;

    private int value;
    @ManyToOne(fetch = FetchType.LAZY)
    private Row row;
    @ManyToOne(fetch = FetchType.LAZY)
    private Column column;
    
    public Cell () {
        value = 0;
    }
    
    public Cell(int value) {
        this.value = value;
    }
    
    public Long getId() {
        return this.id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
}
