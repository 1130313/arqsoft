/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author vitormoreira
 */
@Entity
@Table(name = "ROW")
@SequenceGenerator(name = "ROW_SEQUENCE", sequenceName = "ROW_SEQUENCE", allocationSize = 1, initialValue = 0)
public class Row implements Serializable, Measurable, Countable{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROW_SEQUENCE")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ROW_ID")
    private List<Cell> cells;

    //@ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "dataset_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Dataset dataset;

    public Row() {
        cells = new ArrayList<>();
    }

    public Row(List<Cell> cells) {
        this.cells = cells;
    }
    
    public Long getId() {
        return this.id;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(ArrayList<Cell> cells) {
        this.cells.addAll(cells);
    }

    public int calculteTotal() {
        int total = 0;
        for (Cell cell : this.cells) {
            total+=cell.getValue();
        }
        return total;
    }

    @Override
    public int countNumberOfElements() {
        return this.cells.size();
    }

    @Override
    public List<List<Integer>> getData() {
        List<List<Integer>> data = new ArrayList<>();
        List<Integer> row = new ArrayList<>();
        for (Cell cell : cells) {
            row.add(cell.getValue());
        }
        data.add(row);
        return data;
    }
}
