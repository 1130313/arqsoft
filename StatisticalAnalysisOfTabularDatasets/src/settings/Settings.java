/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Outros
 */
public class Settings {
    private final Properties applicationProperties = new Properties();
    private final static String PROPERTIES_RESOURCE = "statistical/statistical.properties";
    private final static String REPOSITORY_FACTORY_KEY = "persistence.repositoryFactory";


    public Settings() {
        loadProperties();
    }

    public Properties getApplicationProperties() {
        return applicationProperties;
    }

    private void loadProperties() {
        InputStream propertiesStream = null;
        try {
            //propertiesStream = new FileInputStream(PROPERTIES_FILENAME);
            propertiesStream = Settings.class.getClassLoader()
                    .getResourceAsStream(PROPERTIES_RESOURCE);
            if (propertiesStream != null) {
                applicationProperties.load(propertiesStream);
            } else {
                throw new FileNotFoundException("property file '"
                        + PROPERTIES_RESOURCE + "' not found in the classpath");
            }
        } catch (IOException exio) {
            setDefaultProperties();

            Logger.getLogger(Settings.class.getName()).log(
                    Level.SEVERE, null, exio);
        } finally {
            if (propertiesStream != null) {
                try {
                    propertiesStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(Settings.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void setDefaultProperties() {
        applicationProperties.setProperty(REPOSITORY_FACTORY_KEY,
                "persistence.jpa.RepositoryFactoryJPA");
    }

    public String getRepositoryFactory() {
        return applicationProperties.getProperty(REPOSITORY_FACTORY_KEY);
    }
}
