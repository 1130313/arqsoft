/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import Model.Dataset;
import java.util.List;

/**
 *
 * @author Outros
 */
public interface DatasetRepository {
    public List<Dataset> getAll();
    public boolean add(Dataset dataset);
    public void remove(Dataset dataset);
    public void update(Dataset data, Long id);
}
