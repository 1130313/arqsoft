/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import Model.Dataset;
import java.util.logging.Logger;
import settings.Settings;

/**
 *
 * @author Outros
 */
public class Persistence {
    
    public static RepositoryFactory getRepositoryFactory() {

        Settings settings = new Settings();
        String factoryClassName = settings.getRepositoryFactory();
        try {
            return (RepositoryFactory) Class.forName(factoryClassName).
                    newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
        //é preciso compor estas excepções
            return null;
        }
    }
}
