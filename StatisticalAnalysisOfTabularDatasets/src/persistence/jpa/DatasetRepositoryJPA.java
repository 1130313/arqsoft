/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import Model.Dataset;
import Model.Row;
import java.util.List;
import javax.persistence.RollbackException;
import persistence.DatasetRepository;

/**
 *
 * @author Outros
 */
public class DatasetRepositoryJPA extends RepositoryBaseJPA<Dataset, Long> implements DatasetRepository {

    @Override
    public boolean add(Dataset dataset) {
        try {
            super.add(dataset);

        } catch (RollbackException ex) {
            throw new IllegalStateException();
        }
        return true;
    }
    /*
     * TODO: I could do better but I don't know how for now...
     */

    @Override
    public void remove(Dataset dataset) {
        super.delete(dataset);
    }
//
//	@Override
//	public boolean update(Dataset sb, List<Row> rows) {
//		Dataset tmp = new Dataset();
//		tmp.setRows();
//		super.delete(sb);
//		super.add(tmp);
//		return true;
//	}

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public List<Dataset> getAll() {
        return super.all();
    }

    @Override
    public void update(Dataset data, Long id) {
        Dataset toChangeDataset = super.findById(id);
        super.entityManager().getTransaction().begin();
        toChangeDataset.fillDataset(data.getData());
        super.entityManager().getTransaction().commit();
    }

}
