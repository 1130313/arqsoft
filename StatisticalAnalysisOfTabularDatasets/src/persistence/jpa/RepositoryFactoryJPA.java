/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence.jpa;

import persistence.DatasetRepository;
import persistence.RepositoryFactory;

/**
 *
 * @author Outros
 */
public class RepositoryFactoryJPA implements RepositoryFactory {

    public RepositoryFactoryJPA(){
    }
    
    @Override
    public DatasetRepository getDatasetRepository() {
        return new DatasetRepositoryJPA();
    }
    
}
