/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import Model.Dataset;
import java.util.List;

/**
 *
 * @author Outros
 */
public class DatasetAssembler {

    public Dataset transformDatasetDTOIntoDataset(DatasetDTO dto) {
        List<List<Integer>> data = dto.getData();
        Dataset newDataset = new Dataset(data.size(),data.get(0).size());
        newDataset.setName(dto.getName());
        newDataset.fillDataset(data);
        return newDataset;
    }

    public DatasetDTO transformDatasetIntoDTO(Dataset dataset) {
        int rows =  dataset.getRowsSize();
        int cols = dataset.getColumnsSize();
        String name = dataset.getName();
        DatasetDTO dto = new DatasetDTO(rows, cols, name);
        dto.setId(dataset.getId());
        List<List<Integer>> data = dataset.getData();
        dto.fillData(data);
        return dto;
    }
    
}
