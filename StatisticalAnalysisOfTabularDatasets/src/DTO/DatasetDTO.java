/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;

/**
 *
 * @author Outros
 */
public class DatasetDTO {
    
    private final int DEFAULT_VALUE = 0;
    private List<List<Integer>> dataset;
    private String name;
    private Long Id;
    
    
    public DatasetDTO(int rows, int cols , String name)
    {
        this.name = name;
        dataset = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            ArrayList<Integer> newRow = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                Integer newValue = DEFAULT_VALUE;
                newRow.add(newValue);
            }
            this.dataset.add(newRow);
        }
        
    }
    public boolean fillData(List<List<Integer>> data)
    {
        int rows = this.dataset.size();
        int cols = this.dataset.get(0).size();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int value = data.get(i).get(j).intValue();
                this.dataset.get(i).set(j,new Integer(value));
            }
        }
        return true;

    }


    public List<List<Integer>> getData() {
        return this.dataset;
    }
    public String getName(){
        return this.name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }
    
}
