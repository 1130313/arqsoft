/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Outros
 */
public class DatasetDTOCount {

    private DatasetDTO dto;
    int rowNumber;
    int colNumber;

    public DatasetDTOCount(DatasetDTO dto) {
        this.dto = dto;
        this.rowNumber = 0;
        this.colNumber = 0;
    }

    public DatasetDTO getDatasetDTO() {
        return this.dto;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public int getColumnNumber() {
        return colNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public void setColNumber(int colNumber) {

    }

}
