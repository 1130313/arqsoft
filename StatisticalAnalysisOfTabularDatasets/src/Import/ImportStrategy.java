/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import Model.Dataset;
import java.io.File;

/**
 *
 * @author vitormoreira
 */
public interface ImportStrategy {
    
    public Dataset importDataset(File choosenImportFile) throws Exception;
}
