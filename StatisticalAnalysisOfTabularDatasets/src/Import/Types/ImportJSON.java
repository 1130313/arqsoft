/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import.Types;

import Import.ImportStrategy;
import Model.Dataset;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author vitormoreira
 */
public class ImportJSON implements ImportStrategy {

    @Override
    public Dataset importDataset(File choosenImportFile) throws FileNotFoundException {
        Gson gson = new Gson();
        
        Dataset newDataset = gson.fromJson(new FileReader(choosenImportFile), Dataset.class);
        
        return newDataset;
    }
    
}
