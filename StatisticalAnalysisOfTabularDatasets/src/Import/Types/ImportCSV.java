/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import.Types;

import Import.ImportStrategy;
import Model.Dataset;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author vitormoreira
 */
public class ImportCSV implements ImportStrategy {

    private final String DEFAULT_CELL_SEPARATOR = ";";
    
    /**
     * Adapted from https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
     * @param choosenImportFile the file choosen to be imported as a dataset
     * @return a new dataset with the information contained in the file
     * @throws java.io.FileNotFoundException if the file is not found
     */
    @Override
    public Dataset importDataset(File choosenImportFile) throws FileNotFoundException, IOException {
        
        BufferedReader br = new BufferedReader(new FileReader(choosenImportFile));
        
        List<List<Integer>> newDatasetData = new ArrayList<>();
        String line;

        while ((line = br.readLine()) != null) {
            String[] row = line.split(DEFAULT_CELL_SEPARATOR);
            List<String> rowAsStringList = Arrays.asList(row);
            List<Integer> rowAsIntegerList = new ArrayList<>();
            for (int i = 0; i < rowAsStringList.size(); i++) {
                rowAsIntegerList.add(Integer.parseInt(rowAsStringList.get(i)));
            }
            newDatasetData.add(rowAsIntegerList);
        }
            
        Dataset newDataset = new Dataset(newDatasetData.size(), newDatasetData.get(0).size());
        newDataset.fillDataset(newDatasetData);
            
        return newDataset;
    }
    
}
