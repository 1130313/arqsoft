/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import.Types;

import Import.ImportStrategy;
import Model.Dataset;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author vitormoreira
 */
public class ImportXML implements ImportStrategy {

    private final String ELEMENT_TAG_ROW = "row";
    private final String ELEMENT_TAG_CELL = "cell";
    private final String ATTRIBUTE_NAME = "name";
    
    /**
     * Adapted from https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
     * @param choosenImportFile the file to be imported
     * @return a new dataset with the information contained in the xml file
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    @Override
    public Dataset importDataset(File choosenImportFile) throws ParserConfigurationException, 
            SAXException, IOException, NumberFormatException {
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(choosenImportFile);
        
        doc.getDocumentElement().normalize();
        
        List<List<Integer>> newDatasetData = new ArrayList<>();
        
        String datasetName = doc.getDocumentElement().getAttribute(ATTRIBUTE_NAME);
        NodeList nRowList = doc.getElementsByTagName(ELEMENT_TAG_ROW);
        
        // loop through rows
        for (int i = 0; i < nRowList.getLength(); i++) {
            Node nRowNode = nRowList.item(i);
            if (nRowNode.getNodeType() == Node.ELEMENT_NODE) {
                List<Integer> row = new ArrayList<>();
                NodeList nCellList = nRowNode.getChildNodes();
                
                // loop throw cells of current row
                for (int j = 0; j < nCellList.getLength(); j++) {
                    Node nCellNode = nCellList.item(j);
                    if (nCellNode.getNodeType() == Node.ELEMENT_NODE 
                            && nCellNode.getNodeName().equals(ELEMENT_TAG_CELL)) {
                        
                        Element nCellElement = (Element) nCellNode;
                        row.add(Integer.parseInt(nCellElement.getTextContent()));
                    }
                }
                newDatasetData.add(row);
            }
        }
        
        // create dataset, fill it and name it
        Dataset newDataset = new Dataset(newDatasetData.size(), newDatasetData.get(0).size());
        newDataset.fillDataset(newDatasetData);
        newDataset.setName(datasetName);
        
        return newDataset;
    }
    
}
