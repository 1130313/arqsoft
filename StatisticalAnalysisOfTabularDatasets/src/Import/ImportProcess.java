/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import Model.Dataset;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author vitormoreira
 */
public class ImportProcess {
    
    private ImportStrategy m_importStrategy;
    
    public void setImportStrategy(ImportStrategy importStrategy) {
        this.m_importStrategy = importStrategy;
    }
    
    public Dataset beginImportOfDataset(File choosenImportFile) throws Exception {
        return this.m_importStrategy.importDataset(choosenImportFile);
    }
}
