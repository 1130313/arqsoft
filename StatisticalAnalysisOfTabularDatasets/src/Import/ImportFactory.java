/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vitormoreira
 */
public class ImportFactory {
    
    private final String PACKAGE_PREFIX = "Import.Types";
    
    public static ImportFactory createInstance() {
        return new ImportFactory();
    }
    
    public ImportStrategy createExportStrategy(String importStrategyName) {
        
        try {
            Class c = Class.forName(PACKAGE_PREFIX + '.' + importStrategyName);
            ImportStrategy exportStrategy = (ImportStrategy)c.newInstance();
            
            return exportStrategy;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ImportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(ImportFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ImportFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
