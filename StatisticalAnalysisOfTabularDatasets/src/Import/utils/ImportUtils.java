/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Import.utils;

/**
 *
 * @author vitormoreira
 */
public class ImportUtils {
    
    public static enum ImportFileType {
        XML("xml", "ImportXML"), CSV("csv", "ImportCSV"), JSON("json", "ImportJSON");
        
        private final String m_extention;
        private final String m_className;
        
        ImportFileType(String ext, String className) {
            this.m_extention = ext;
            this.m_className = className;
        }
        
        public static String retrieveImportClassName(String ext) {
            for (ImportFileType type : ImportFileType.values()) {
                if(type.m_extention.equals(ext)) return type.m_className;
            }
            return null;
        }
    }
    
    public static String getFileExtension(String fileName) {
        String fileExt = null;
        
        int i = fileName.lastIndexOf('.');
        if (i >= 0)
            fileExt = fileName.substring(i+1);

        return fileExt;
    }
}
